var API_SERVER = 'http://elections.huffingtonpost.com',
	API_BASE = '/pollster/api/',
	API_FILE = 'polls.json',
	callback = '?callback=pollsterPoll',
	params = '&topic=2016-president',
	latest_data,
	last_updated,
	pollster;

window.pollsterPoll = function (incoming_data) {
	latest_data = incoming_data;
	console.log(incoming_data);

	$.each(latest_data, function (index, value) {
		if (value.pollster === "Morning Consult") {
			buildTabs(value.questions);
			visualizeDemocrates();
		}
	});
};

$(document).ready(function () {
	$.ajax({
		url: API_SERVER + API_BASE + API_FILE + callback + params,
		dataType: 'script',
		type: 'GET',
		cache: true
	});
});

function visualizeDemocrates() {
	var data = latest_data[1],
		last_updated = data.last_updated,
		pollster = data.pollster,
		question = data.questions[1],
		subpopulation = question.subpopulations[0],
		responses = subpopulation.responses;
	$("#" + question.chart).append(question.name + "</br>");
	$.each(responses, function (index, response) {
		$("#" + question.chart).append(response.choice + " Value: " + response.value + "</br>");
	});

	$("#" + question.chart).append(pollster + "</br>");
}

function tabTemplate(question) {
	return "<li><a href='#" + question.chart + "'>" + question.name + "</a></li>";
}

function tabDivTemplate(question) {
	return "<div id='" + question.chart + "'><canvas id='" + question.chart + "_chart'></canvas></div>";
}

function buildTabs(questions) {
	$.each(questions, function (index, value) {
		$('.tab_heads').append(tabTemplate(value));
		$(tabDivTemplate(value)).insertAfter(".tab_heads");
	});
	$('.tabs').tabs();
}