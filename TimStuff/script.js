var API_SERVER = 'http://elections.huffingtonpost.com',
    API_BASE = '/pollster/api/',
    API_FILE = 'polls.json',
    callback = '?callback=pollsterPoll',
    params = '&state=US&topic=2016-president',
    latest_data;

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

window.pollsterPoll = function(incoming_data){
    latest_data = incoming_data;
    visualizeRightWrong();
};

$(document).ready(function(){
    $.ajax({
        url: API_SERVER + API_BASE + API_FILE + callback + params,
        dataType: 'script',
        type: 'GET',
        cache: true
    });
});

function visualizeRightWrong(){
    var outputString = "";
    var rightWrongCharts = [];
    for(var index = 0; index < latest_data.length; index++){
        for(var innerIndex = 0; innerIndex < latest_data[index].questions.length; innerIndex++){
            if(latest_data[index].questions[innerIndex].name == "US Right Direction Wrong Track"){
                var addedObject = latest_data[index].questions[innerIndex];
                addedObject.method = latest_data[index].method;
                addedObject.partisan = latest_data[index].partisan;
                addedObject.pollster = latest_data[index].pollster;
                rightWrongCharts.push(addedObject);
            }
        }
    }
    console.log(rightWrongCharts);
    ctx.font = "20px Arial";
    ctx.fillText("Is America on the right path? Or the wrong track?", 10, 25);
    ctx.font = "18px Arial";
    ctx.fillText("Right path", 10, 100);
    ctx.fillStyle = "green";
    ctx.fillRect(10, 125, (rightWrongCharts[0].subpopulations[0].responses[0].value)*5, 20);
    ctx.fillStyle = "black";
    ctx.fillText("Wrong track", 10, 200);
    ctx.fillStyle = "red";
    ctx.fillRect(10, 225, (rightWrongCharts[0].subpopulations[0].responses[1].value)*5, 20);
    ctx.fillStyle = "black";
    ctx.fillText("Undecided", 10, 300);
    ctx.fillStyle = "blue";
    ctx.fillRect(10, 325, (rightWrongCharts[0].subpopulations[0].responses[2].value)*5, 20);
    ctx.fillStyle = "black";
    ctx.font = "12px Arial";
    ctx.fillText("This " + rightWrongCharts[0].partisan + " poll from " + rightWrongCharts[0].pollster + " collected info from " + rightWrongCharts[0].method, 50, canvas.height-10);
}